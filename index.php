<?php
    echo "Hewan Beserta Ciri Cirinya". "<br>". "<br>";
    require("animal.php");
    require("Ape.php");
    require("Frog.php");


    $sheep = new Animal("shaun");
    echo "Nama : " . $sheep->name .  "<br>"; // "shaun"
    echo "Jumlah Kaki : " . $sheep->legs .  "<br>"; // 2
    echo $sheep->hasil; // false

    echo "<br>" . "<br>";
    $sungokong = new Ape("Kera Sakti");
    echo "Nama : " .$sungokong->name . "<br>";
    echo  $sungokong->yell() . "<br>"; // "Auooo"

    echo "<br>" . "<br>";
    $frog = new Frog("Katak");
    echo "Nama : " .$frog->name . "<br>";
    echo "Jumlah Kaki : " . $frog->legs .  "<br>";
    echo  $frog->jump() . "<br>"; // "hop hop"


?>